# RecipeListApplication

Recipe List Application - Without Storyboard

# IMPORTANT #

Please look over this README before opening/running application on XCode. 

Since we are using Cocoapods, the developer must install the necessary pods or dependencies before opening the project’s .xcworkspace. 

# Getting Started #

Open Terminal.

To clone this repository into your computer do the following commands:

First, cd into wherever you want to have this project:

Then, 

```bash

git clone <HTTPS_URL_AT_THE_TOP_OF_PAGE>

cd [FOLDER_OF_REPOSITORY]
```

*This will automatically set your current branch to master*

**OR**

To clone repository and automatically go to specific branch:

```bash
git clone -b <EXISTING_BRANCH> <HTTPS_URL_AT_THE_TOP_OF_PAGE>
cd [FOLDER_OF_REPOSITORY]
```

# GIT HACKS & TIPS #

Using version control is our best bet in keeping everything organized and clean.

*NOTE: Anywhere it says <something>, < > means it is a placeholder. Don't include the < > when you type a command please.*

## Branches ##
**Make sure you are NEVER working on the master branch**

To check what branch you are on:

```bash
git branch

```

To create and switch to a new branch:

```bash
git checkout -b <NAME_OF_NEW_BRANCH>
```
** It's good practice to name branches in a way that it one-two words describing your current task.**

*For example: git checkout -b MainMenu_NavigationFlow*

To switch to an existing branch:

```bash
git checkout <NAME_OF_EXISTING_BRANCH>
```

To delete an existing local branch:

```bash
git branch -d <NAME_OF_EXISTING_BRANCH>
```

To delete an existing remote branch (not recommended):

```bash
git push origin --delete <NAME_OF_BRANCH>
```

## Commiting Changes ##

So when you are ready to push your changes to your remote branch, there are few things you need to do:

First, check which files you have modified:

```bash
git status
```

* Any green files have been saved to your current commited state. Any red files still need to be saved.

To save all red files:

```bash
git add .
```

To save a specific red file:

```bash
git add /path/to/file
```

Second, once all the files you want to commit are green, you can do the following:

```bash
git commit
```

**or**

```bash
git commit -m "COMMIT_MESSAGE"
```

For context:

The first one will allow you to see your commit and let you change the commit message there.

The second is a shortcut so you don't see your whole commit and you set the message inline.

* -m means this commit will have a message

* Always write your message inside quotation marks for best practices.

*Please make the commit message as descriptive but concise as you can.*


## Pushing ##

Once you have commited your changes, you want to push the commit to our remote repository so we can all have access to it.

The first time you push a local branch that does not have a corresponding remote branch, do the following:

```bash
git push origin --set-upstream <BRANCH_NAME>
```

Once you do that, whenever you push within the same branch you can simply do:

```bash
git push
```

and it will know to push to that specific remote branch.


** NOTE: You will have to use --set-upstream for any new branch you create and want to push to our remote repo. **
## HISTORY ##

If using git via terminal, a really cool tool to have is the ability to see a tree-like visual of the project's commits. 
This is optional but if interested...

Using vi/vim or emacs, open up the git config file for the project located in the .git directory.

If you go into the directory that has this README file, and type:

```bash
ls -la # lists all files in a list	
```

If in the right directory, you should be able to see a .git directory.

```bash
vi .git/config

```

and you should be inside of the git config file. This is where you can create global or repository options. 

Add the following to your config file, making sure you don't change anything else.

```bash

[alias]
	history = log --all --graph --pretty=format:'%C(magenta)%h%Creset -%C(yellow)%d%Creset%s%Cgreen(%cr) %C(bold blue)</%an>%Creset' --abbrev-commit --date=relative

```

Now anywhere within the scope of this repository, you can type:

```bash
git history

```

and a really nice color-coded graph will appear displaying all commits/merges/pulls.


# Required Cocoapods #

To install cocoapods:

```bash 
sudo gem install cocoapods
cd [FOLDER_OF_PROJECT]
pod install
```

That should get you all of the dependencies.
** Please always open and make changes to the .xcworkspace, NOT .xcodeproj **

To open workspace via terminal:

```bash
open RecipeAppBase.xcworkspace
```

If it doesn't open up using Xcode:

```bash
open -a Xcode RecipeAppBase.xcworkspace
```