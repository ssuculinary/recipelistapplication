//
//  ViewController.swift
//  RecipeAppBase
//
//  Created by Sugar Palaces on 4/24/18.
//  Copyright © 2018 Culinary Services. All rights reserved.
//

import UIKit
import SnapKit

class MainViewController: UIViewController {
    fileprivate var searchButton: UIButton?
    fileprivate var favoritesButton: UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        // This is how you change the background color of a view controller
        // Right Hand Side (RHS): some Swift built-in functions accept enums as values. For example, here .grey is an enum that represents the UIColor gray on the backend.
        self.view.backgroundColor = .gray

        // Since we set the UI components as optional above, we have to create them...
        searchButton = UIButton()
        favoritesButton = UIButton()
        // ...as well as check here that they are not null. This is a safeguard check. If searchButton or favoritesButton weren't created, the if statement below would fail and
        // the application wouldn't crash.
        if let search = searchButton, let favorite = favoritesButton {
            // Now that we have dereferenced the button, we can use search and favorites to handle the buttons, respectively.
            // Arbitrary constant to be the gap between the buttons on the Y-axis
            let buttonGap = self.view.bounds.height * 0.15
            // In order to reference/customize any UI component, it has to be added to the parent view. In this case, we want searchButton and favoritesButton to be on MainViewController, so we add it so its view.
            // self refers to MainViewController
            self.view.addSubview(search)
            self.view.addSubview(favorite)
            // Using SnapKit, we can arrange the buttons on the controller's view.
            // This library is really freaking cool because it makes the task of UI layout so much easier. And the syntax makes it very easy to understand what is going on.
            search.snp.makeConstraints { (make) in
                make.height.equalToSuperview().multipliedBy(0.10) // Makes height equal to its superview (controller's view since we added it to the self.view up in line 35). And then it takes 10% of the height.
                make.width.equalToSuperview().multipliedBy(0.25)
                make.centerX.equalToSuperview()                   // Makes the center x of the button to the center x of the controller's view.
                make.centerY.equalToSuperview().offset(-(buttonGap)) // Makes the center y of the button to the center y of the controller's view but with an offset.
            }
            favorite.snp.makeConstraints { (make) in
                make.height.equalToSuperview().multipliedBy(0.10)
                make.width.equalToSuperview().multipliedBy(0.25)
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview().offset(buttonGap)
            }

            // Button Cutomization
            search.setTitle("Search", for: .normal)
            search.backgroundColor = .black
            search.tintColor = .white
            search.addTarget(self, action: #selector(self.searchClicked), for: .touchUpInside) // When the user presses the search button, we want there to be an action. This action will be handled in a separate function.

            favorite.setTitle("Favorites", for: .normal)
            favorite.backgroundColor = .black
            favorite.tintColor = .white
            favorite.addTarget(self, action: #selector(self.favoritesClicked), for: .touchUpInside)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    @objc fileprivate func searchClicked(_ button: UIButton) {
        // Here we create an instance of SearchViewController
        let searchVC = SearchViewController()
        // This first checks if our current view controller has a navigation controller.
        // If yes, then it will push searchVC onto its stack.
        self.navigationController?.pushViewController(searchVC, animated: true)
        /*
         If we didn't want to push it onto the stack and just replace the view controller,
         we could use self.present(searchVC, animated: true, completion: nil)
         */
    }

    @objc fileprivate func favoritesClicked(_ button: UIButton) {
        let favoritesVC = FavoritesTableViewController()
        self.navigationController?.pushViewController(favoritesVC, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

