//
//  RecipeServices.swift
//  RecipeAppBase
//
//  Created by Dulce Palacios on 5/5/18.
//  Copyright © 2018 Culinary Services. All rights reserved.
//

import UIKit
import SwiftyJSON

class RecipeServices: BaseServices {
    static let shared = RecipeServices()

    override init() {
        super.init()
        self.pathURL = "search"
    }

    func serializeRecipeJSON(_ result: ServiceCallResult) -> [Recipe] {
        if let data = result.rawJSON["hits"].array {
            let recipes = data.map( { Recipe(with: $0["recipe"]) } )
            return recipes
        }
        return []
    }

    func getRecipes(from query: String, success: @escaping SuccessBlock, failure: @escaping FailureBlock) {
        self.pathURL += "?q=\(query)"
        self.pathURL += "&app_id=\(self.appID)&app_key=\(self.appKey)"
        getFromURL(success: { (result) in
            let recipes = self.serializeRecipeJSON(result)
            result.serializedData = recipes
            success(result)
        }, failure: failure)
        self.pathURL = "search"
    }
}
