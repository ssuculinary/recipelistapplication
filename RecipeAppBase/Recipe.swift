//
//  Recipe.swift
//  RecipeAppBase
//
//  Created by Sugar Palaces on 5/3/18.
//  Copyright © 2018 Culinary Services. All rights reserved.
//

import UIKit
import SwiftyJSON

enum DietPreferences: String {
    case balanced = "Balanced"
    case highProtein = "High-Protein"
    case highFiber = "High-Fiber"
    case lowFat = "Low-Fat"
    case lowCarbs = "Low-Carb"
    case lowSodium = "Low-Sodium"

    func toDashFreeString() -> String {
        switch self {
        case .balanced: return "Balanced"
        case .highProtein: return "High Protein"
        case .highFiber: return "High Fiber"
        case .lowFat: return "Low Fat"
        case .lowCarbs: return "Low Carb"
        case .lowSodium: return "Low Sodium"
        }
    }
}

enum HealthRestrictions: String {
    case vegan = "Vegan"
    case vegetarian = "Vegetarian"
    case paleo = "Paleo"
    case dairyFree = "Dairy-Free"
    case glutenFree = "Gluten-Free"
    case wheatFree = "Wheat-Free"
    case fatFree = "Fat-Free"
    case lowSugar = "Low-Sugar"
    case eggFree = "Egg-Free"
    case peanutFree = "Peanut-Free"
    case treeNutFree = "Tree-Nut-Free"
    case soyFree = "Soy-Free"
    case fishFree = "Fish-Free"
    case shellfishFree = "Shellfish-Free"

    func toDashFreeString() -> String {
        switch self {
        case .vegan: return "Vegan"
        case .vegetarian: return "Vegetarian"
        case .paleo: return "Paleo"
        case .dairyFree: return "Dairy Free"
        case .glutenFree: return "Gluten Free"
        case .wheatFree: return "Wheat Free"
        case .fatFree: return "Fat Free"
        case .lowSugar: return "Low Sugar"
        case .eggFree: return "Egg Free"
        case .peanutFree: return "Peanut Free"
        case .treeNutFree: return "Tree Nut Free"
        case .soyFree: return "Soy Free"
        case .fishFree: return "Fish Free"
        case .shellfishFree: return "Shellfish Free"
        }
    }
}

class Recipe: NSObject {
    public enum Keys {
        public static let titleKey              = "label"
        public static let imageURLKey           = "image"
        public static let sourceKey             = "source"
        public static let urlKey                = "url"
        public static let servingsKey           = "yield"
        public static let caloriesKey           = "calories"
        public static let ingredientsKey        = "ingredients"
        public static let dietKey               = "dietLabels"
        public static let healthKey             = "healthLabels"
    }

    var title: String
    var image: UIImage?
    var source: String
    var url: String
    var servings: Int
    var calories: Float
    var ingredients: [Ingredient]
    var diet: [DietPreferences]
    var health: [HealthRestrictions]
    fileprivate var imageURL: String?

    override init() {
        self.title = ""
        self.imageURL = ""
        self.source = ""
        self.url = ""
        self.servings = 0
        self.calories = 0.0
        self.ingredients = []
        self.diet = []
        self.health = []
    }

    init(title: String, imageURL: String?, image: UIImage?, source: String, url: String, servings: Int, calories: Float, ingredients: [Ingredient], diet: [DietPreferences], health: [HealthRestrictions]) {
        self.title = title
        self.imageURL = imageURL
        self.image = image
        self.source = source
        self.url = url
        self.servings = servings
        self.calories = calories
        self.ingredients = ingredients
        self.diet = diet
        self.health = health
    }

    init(with json: JSON ) {
        print("json: \(json)")
        self.title = json[Keys.titleKey].string ?? ""
        self.source = json[Keys.sourceKey].string ?? ""
        self.url = json[Keys.urlKey].string ?? ""
        self.servings = json[Keys.servingsKey].int ?? 0
        self.calories = json[Keys.caloriesKey].float ?? 0.0
        self.ingredients = []
        if let ingredients_json_array = json[Keys.ingredientsKey].array {
            for ingredient_json in ingredients_json_array {
                self.ingredients.append(Ingredient(with: ingredient_json))
            }
        }
        self.diet = []
        print("diet: \( json[Keys.dietKey].array)")
        if let diet_json_array = json[Keys.dietKey].array {
            for diet_string in diet_json_array {
                if let literal = diet_string.string, let diet_enum = DietPreferences(rawValue: literal) {
                    self.diet.append(diet_enum)
                }
            }
        }

        self.health = []
        if let health_json_array = json[Keys.healthKey].array {
            for health_string in health_json_array {
                if let literal = health_string.string, let health_enum = HealthRestrictions(rawValue: literal) {
                    self.health.append(health_enum)
                }
            }
        }

        self.image = nil
        if let imgUrlStr = json[Keys.imageURLKey].string, let imgUrl = URL(string: imgUrlStr), let data = try? Data(contentsOf: imgUrl)  {
            self.image = UIImage(data: data)
        }
        super.init()
        self.printInfo()
    }

    func printInfo() {
        print("Recipe: \(self.title)")
        print(" ")
        print("     imageURL: \(imageURL)")
        print("     image: \(image)")
        print("     source: \(source)")
        print("     url: \(url)")
        print("     servings: \(servings)")
        print("     calories: \(calories)")
        print("     ingredient: ")
        for ing in ingredients {
            ing.printInfo()
        }
        print("diet: ")
        for d in diet {
            print("Enum: \(d.toDashFreeString())")
        }
        print("health: ")
        for h in health {
            print("Enum: \(h.toDashFreeString())")
        }
        print("")
    }
}
