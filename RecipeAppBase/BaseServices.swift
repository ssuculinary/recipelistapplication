//
//  BaseServices.swift
//  RecipeAppBase
//
//  Created by Sugar Palaces on 5/3/18.
//  Copyright © 2018 Culinary Services. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

typealias SuccessBlock = (_ result: ServiceCallResult) -> Void
typealias FailureBlock = (_ error: Error) -> Void

class ServiceCallResult {
    var rawJSON: JSON = JSON.null
    var statusCode: Int?
    var serializedData: Any?
    var caller: BaseServices

    init(caller: BaseServices) {
        self.caller = caller
    }
}

class BaseServices {
    var baseURL: String
    var pathURL: String
    var appID: String
    var appKey: String

    init() {
        baseURL = "https://api.edamam.com/"
        pathURL = ""
        appID   = "c876b873"
        appKey  = "ac417bb2917f2b029ff88f77c2ba689b"
    }

    func getFromURL(success: @escaping  SuccessBlock, failure: @escaping FailureBlock) {
        let base = URL(string: baseURL)
        let url = URL(string: pathURL, relativeTo: base)
        let result = Alamofire.request(url!)
        result.responseJSON { (response) in
//            debugPrint(response)
            switch response.result {
            case .success:
                let result = self.buildServiceCallResult(response)
                success(result)
            case .failure:
                print("ERROR IN RESPONSE: \(String(describing: response.response))")
                if let error = response.error {
                    failure(error)
                } else {
                    print("ERROR: NO ERROR FOUND")
                }
            }
        }
    }

    fileprivate func buildServiceCallResult<T>(_ response: DataResponse<T>) -> ServiceCallResult {
        let result = ServiceCallResult(caller: self)
        if let httpResponse = response.response { result.statusCode = httpResponse.statusCode}
        if let data = response.result.value {
//            print("data: \(data)")
            result.rawJSON = JSON(data)
        }
        return result
    }
    
}
