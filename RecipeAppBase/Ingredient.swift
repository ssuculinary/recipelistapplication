//
//  Ingredient.swift
//  RecipeAppBase
//
//  Created by Sugar Palaces on 5/3/18.
//  Copyright © 2018 Culinary Services. All rights reserved.
//

import UIKit
import SwiftyJSON

class Ingredient: NSObject {
    public enum Keys {
        public static let textKey       = "text"
        public static let weightKey   = "weight"
    }

    var weight: Float?
    var name: String?

    override init() {
        self.name = nil
        self.weight = 0.0
    }

    init(name: String, weight: Float) {
        self.name = name
        self.weight = weight
    }

    init(with json: JSON) {
        self.name = json[Keys.textKey].string
        self.weight = json[Keys.weightKey].float
    }

    func printInfo() {
        print("Ingredient: \(self.name ?? "NO NAME")")
        print("           weight: \(self.weight)")
    }
}
