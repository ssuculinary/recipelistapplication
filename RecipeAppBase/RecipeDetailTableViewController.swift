//
//  RecipeDetailTableViewController.swift
//  RecipeAppBase
//
//  Created by Sugar Palaces on 5/8/18.
//  Copyright © 2018 Culinary Services. All rights reserved.
//

import UIKit

enum DetailSections: Int {
    case image = 0
    case label
    case diet
    case health
    case ingredients
    case numSections // Sweet work-around to get all number of cases in the enum -- useful when figuring out number of sections for tableview
}

class RecipeDetailTableViewController: UITableViewController {

    fileprivate var recipe: Recipe
    fileprivate var cellIdentifier = "detail"

    init(with recipe: Recipe) {
        self.recipe = recipe
        super.init(style: .grouped)
    }

    required init?(coder aDecoder: NSCoder) {
        self.recipe = Recipe()
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        self.tableView.backgroundColor = .white
        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return DetailSections.numSections.rawValue // ,raw-value returns the integer representation of the enum. Will give you a compile error if you don't include it.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = DetailSections(rawValue: section) else { return 0 } // An enum init returns an optional
        switch section {
        case .image: return 1
        case .label: return 1
        case .diet: return self.recipe.diet.count
        case .health: return self.recipe.health.count
        case .ingredients: return self.recipe.ingredients.count
        default: return 0 // switch must be exhaustive
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = DetailSections(rawValue: indexPath.section) else { return 0.0 }
        switch section {
        case .image: return 200 // Try to never hard code -- just for the sake of the example
        default: return 44
        }
    }

    /*
     This is not the best way to implement this. This is just to show you that it can be done with a generic UITableViewCell.

     There is an appparent bug in this. When there are more than 8 items for ingredients / any list, the cells appaear to overlap and repeat.

     This is because we have not overriden the prepareForReuse function. BUT it is best that you do the following:
     - Create a generic subclass of UITableViewCell that will satisfy your UI requirements.
     - In that subclass, override prepareForReuse and clear the cell out. 
    */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        if let section = DetailSections(rawValue: indexPath.section) {
            let row = indexPath.row
            switch section {
            case .image:
                    cell.imageView?.image = recipe.image
                    cell.imageView?.contentMode = .scaleAspectFit
                    cell.imageView?.snp.makeConstraints({ (make) in
                        make.centerX.equalToSuperview()
                        make.top.equalToSuperview()
                        make.bottom.equalToSuperview()
                        make.left.equalToSuperview()
                        make.right.equalToSuperview()
                    })
                    cell.backgroundColor = .clear
            case .diet: cell.textLabel?.text = recipe.diet[row].toDashFreeString()
            case .health: cell.textLabel?.text = recipe.health[row].toDashFreeString()
            case .ingredients: cell.textLabel?.text = recipe.ingredients[row].name
            case .label:
                    cell.textLabel?.text = recipe.title
                    cell.textLabel?.textAlignment = .center
            default:
                cell.textLabel?.text = nil
                cell.imageView?.image = nil
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let section = DetailSections(rawValue: section) else { return nil }
        switch section {
        case .diet: return "Diet Preferences" // Try to never hardcore
        case .health: return "Health Restrictions"
        case .ingredients: return "Ingredients"
        default: return nil
        }
    }
}


