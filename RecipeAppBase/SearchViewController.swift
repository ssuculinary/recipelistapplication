//
//  SearchViewController.swift
//  RecipeAppBase
//
//  Created by Sugar Palaces on 5/3/18.
//  Copyright © 2018 Culinary Services. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    // This explanation mark at the end is saying "object will be created before using it." This is called an implicit optional and is not as safe as a regular optional (?) because it has a greater chance of crashing your application.
    fileprivate var searchBar: UISearchBar!
    fileprivate var tableView: UITableView!

    fileprivate var search_query: String?
    fileprivate var search_results: [Recipe] = []

    fileprivate let cellIdentifier = "results"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Create UI components
        searchBar = UISearchBar()
        tableView = UITableView(frame: CGRect.zero, style: .grouped)

        // Instead of putting everything in the viewDidLoad, you can split the actions into functions. This way is useful when you have a lot of UIComponents or background work that you need to do. It makes it clean and readable.
        addViews()
        setupConstraints()
        setupAttributes()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    fileprivate func addViews() {
        self.view.addSubview(searchBar)
        self.view.addSubview(tableView)
    }

    fileprivate func setupConstraints() {
        // If there is a navigation bar, we want to make sure our items are underneath it so that they are visible and accessible to the user. You must always consider the status bar height in your calculations.
        var topOffset = UIApplication.shared.statusBarFrame.height
        if let nav = self.navigationController?.navigationBar {
            topOffset += nav.frame.height
        }
        // Setting the search bar to the top of the view, but under the navigation bar
        self.searchBar.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(topOffset)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.10)
        }
        // Setting the table view under the search bar
        self.tableView.snp.makeConstraints { (make) in
            // SnapKit makes it easy to access the "bottom/top/left/right" of other objects that have also been constrained with it.
            make.top.equalTo(searchBar.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }

    fileprivate func setupAttributes() {
        // UISearchBar has a lot of customizable attributes, feel free to play around with them.
        // Documentation: https://developer.apple.com/documentation/uikit/uisearchbar
        self.searchBar.autocapitalizationType = .words
        self.searchBar.showsSearchResultsButton = true
        self.searchBar.returnKeyType = .done
        self.searchBar.delegate = self

        // [VERY IMPORTANT] The following two lines are what connect our instance of UITableView to this class' delegate and datasouce functions defined below. If we don't include them, nothing we do in our numberInSection, numberOfRowsInSection, cellForRowAt, didSelectRowAt, etc. will not work.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsMultipleSelection = false
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }

    func recipeSearch(with query: String) {
        // Service call to get recipes using query from search bar input.
        RecipeServices.shared.getRecipes(from: query, success: { (result) in
            // If we found recipes, then we want to update our search_results array and update the table to display the results.
            if let recipes = result.serializedData as? [Recipe] { // Have to type cast it to [Recipe] because serializedData is cast as AnyObject in order to have reusability
                self.search_results = recipes
                self.tableView.reloadData() // Updated table data and inturn, the display.
            }
        }) { (error) in
            print("error: \(error)")
        }
    }

    // MARK: - Table View Data Source & Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1 // This can be any number and can also use a dynamic variable. For our sake, we only need one section to hold the results.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.search_results.count // This variable can change depending on how many results were returned from the service call. The table will update accordingly.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as UITableViewCell
        cell.textLabel?.text = self.search_results[indexPath.row].title
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Section selected: \(indexPath.section) -- Row selected: \(indexPath.row)")
        print("Item Selected: \(self.search_results[indexPath.row].title)")
        let detailVC = RecipeDetailTableViewController(with: self.search_results[indexPath.row])
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    // MARK: - Search Bar Delegate
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        guard let query = self.search_query else { return }
        recipeSearch(with: query)
        self.search_query = nil
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = self.search_query else { return }
        recipeSearch(with: query)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.search_query = searchText
        self.searchBar.showsSearchResultsButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
